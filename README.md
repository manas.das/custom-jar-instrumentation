# custom-jar-instrumentation
The code base use to scan decompiled java code base and scan for specific tokens in the code base. 
With the current code base trying to trace all possible MySQL 5 DB connections made from all the Custom Jars code base.
Script which walks through decompiled jar files and look for token like DriverManager.getConnection and report. We can 
provide multiple tokens and collect result by File Name. Script has the capability to filter by inclusion & exclusion
rules like following.
- Jar name starts with (e.g. "ojdbc", "mysql-connector", "db2jcc", "postgres", "nzjdbc", "jconn4")
- File extension (e.g. properties)
- Java package namespace in case of java file (e.g. "/com/ibm/as400", "/org/apache/commons", "/oracle/jdbc/rowset", 
- "/org/apache/ibatis", "/com/mysql/cj", "/com/mysql/jdbc", "/org/apache/logging", "/org/apache/log4j")

It creates the output in JSON and schema looks like the following. 
```
root
|-- jarName: string (nullable = true)
|-- jarPath: string (nullable = true)
|-- path: string (nullable = true)
|-- tenantName: string (nullable = true)
|-- tokenOccurrences: array (nullable = true)
|    |-- element: struct (containsNull = true)
|    |    |-- line: long (nullable = true)
|    |    |-- token: string (nullable = true)
```

## How to run
Run Main class. Reports will be generated under `logs` directory.

## Spark Shell script code to convert the output from JSON to CSV
```sparksql
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions.UserDefinedFunction

//Load into Dataframe
val df = spark.read.json("<absolute_path_to_custom-jar-instrumentation>/logs/info.log");
       
// Define a UDF to stringify the array<struct<line:bigint,token:string>> into a String
val mergeStructsUDF: UserDefinedFunction = udf((data: Seq[(Long, String)]) => {
      data.map { case (line, token) =>
        s"line=$line | token=$token"
      }.mkString("; ")
    })

// Create a new column "merged_token_occurrence" using the UDF
df.withColumn("merged_token_occurrence", mergeStructsUDF(col("tokenOccurrences")))
.drop("tokenOccurrences")
.write
.option("header", true)
.csv("<absolute_path_to_custom-jar-instrumentation>/logs/info.csv")
```
