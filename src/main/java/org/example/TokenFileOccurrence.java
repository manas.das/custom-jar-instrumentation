package org.example;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;


@Setter
@Getter
/**
 * class TokenFileOccurrence is used for se
 */
public class TokenFileOccurrence implements Serializable {

    private static final long serialVersionUID = 1L;
    private String tenantName;
    private String path;
    private String jarPath;
    private String jarName;
    private List<TokenOccurrence> tokenOccurrences;

    public TokenFileOccurrence(String path, String tenantName, String jarPath, String jarName, List<TokenOccurrence> tokenOccurrences) {
        this.tenantName = tenantName;
        this.path = path;
        this.jarPath = jarPath;
        this.jarName = jarName;
        this.tokenOccurrences = tokenOccurrences;
    }


    static class TokenOccurrence implements Serializable {
        private static final long serialVersionUID = 1L;
        String token;
        int line;


        public TokenOccurrence(String token, int line) {
            this.token = token;
            this.line = line;
        }
    }

}
