package org.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.gson.Gson;

public class Main {


    private Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws IOException {
        final String parentPath = "/Users/dasmanas/Downloads/out2";
        String[] searchFor = {"DriverManager.getConnection"
                //        ,"externalconfig.properties", "Class.forName(\"com.mysql.jdbc.Driver\")"
        };
        String[] excludeJarNames = {"ojdbc", "mysql-connector", "db2jcc", "postgres", "nzjdbc", "jconn4"
                , "habeans", "postgis-jdbc", "MySqlJdbcDriver", "Text_JDBC"};
        String[] excludeFileExtensions = {"properties"};
        String[] excludePkgNs = {
                "/com/ibm/as400"
                , "/com/ibm/mm/sdk"
                , "/com/ibm/db2"
                , "/org/apache"
                //, "/org/apache/commons"
                //, "/org/apache/log4j"
                //, "/org/apache/ibatis"
                //, "/org/apache/logging"
                , "/oracle/jdbc/rowset"
                , "/com/mysql"
                //, "/com/mysql/cj"
                //, "/com/mysql/jdbc"
                , "/com/ibm/eNetwork"};

        int parentPathCharLength = parentPath.length();
        Path path = Paths.get(parentPath);

        Main main = new Main();
        List<Path> paths = main.listFiles(path, excludeFileExtensions);
        /*String[] searchFor = {"java.sql.Connection", "java.sql.DriverManager", "javax.sql.DataSource", "Connection",
                "DriverManager", "DataSource", "mysql-connector-java", "com.mysql.jdbc.Driver",
                "DriverManager.getConnection"};*/
        //String[] searchFor = {"DriverManager.getConnection", "Class.forName(\"com.mysql.jdbc.Driver\")"};

        /*
        // There are a couple of ways to control
        // Either use
        -Djava.util.concurrent.ForkJoinPool.common.parallelism=8
        // or use
        ForkJoinPool myPool = new ForkJoinPool(8);
        myPool.submit(() ->
                list.parallelStream().forEach(*//* Do Something *//*);).get();
         */
        paths.parallelStream().forEach(x -> main.searchTokens(searchFor, x, parentPathCharLength, excludeJarNames, excludePkgNs));
    }

    public List<Path> listFiles(Path path, String[] excludeFileExtensions) throws IOException {
        List<Path> result;
        try (Stream<Path> walk = Files.walk(path)) {
            result = walk
                    .filter(Files::isRegularFile)
                    .filter(p -> Arrays.stream(excludeFileExtensions).noneMatch(extn -> p.getFileName().toString().toLowerCase().endsWith(extn)))
                    .collect(Collectors.toList());
        }
        return result;
    }

    public String getPackageNameSpaceForJavaFile(String pathStartWithCustom, String fileExtension, int jarNameEndIndex) {
        if (fileExtension != null && fileExtension.equalsIgnoreCase("java")) {
            int pnsEnd = pathStartWithCustom.lastIndexOf("/");
            if (pnsEnd != -1 && pnsEnd < pathStartWithCustom.length() - 1) {
                return pathStartWithCustom.substring(jarNameEndIndex, pnsEnd);
            }
        }
        return null;
    }

    public String getTenantName(String absPathStr, int parentPathCharLength) {
        try {
            int endIndexTenant = absPathStr.indexOf("/", parentPathCharLength + 1);
            if (endIndexTenant > -1) {
                return absPathStr.substring(parentPathCharLength + 1, endIndexTenant);
            }
            return absPathStr.substring(parentPathCharLength + 1);
        } catch (Exception e) {
            return null;
        }
    }

    public void searchTokens(String[] searchFor, Path path, int parentPathCharLength, String[] excludeJarNames, String[] excludePkgNs) {

        String absPathStr = path.toAbsolutePath().toString();
        String tenantName = getTenantName(absPathStr, parentPathCharLength);
        String fileName = path.getFileName().toString();
        String fileExtension = null;
        int lastDotIndex = fileName.lastIndexOf('.');
        if (lastDotIndex > 0 && lastDotIndex < fileName.length() - 1) {
            fileExtension = fileName.substring(lastDotIndex + 1);
        }
        String pathStartWithCustom = absPathStr.substring(parentPathCharLength);
        int indexOfJar = pathStartWithCustom.indexOf(".jar");
        String jarPath = null;
        String jarName = null;
        if (indexOfJar != -1) {
            int jarNameBeginIndex = pathStartWithCustom.lastIndexOf("/", indexOfJar) + 1;
            int jarNameEndIndex = pathStartWithCustom.indexOf("/", indexOfJar);
            if (jarNameEndIndex != -1) {
                jarPath = pathStartWithCustom.substring(0, jarNameEndIndex);
                jarName = pathStartWithCustom.substring(jarNameBeginIndex, jarNameEndIndex);
                //Excluding specific jar name
                if (jarName != null && !jarName.isEmpty() && Arrays.stream(excludeJarNames).anyMatch(jarName::contains)) {
                    return;
                }

                //Excluding specific java file under specific package name specifier
                String javaFilePkgNs = getPackageNameSpaceForJavaFile(pathStartWithCustom, fileExtension, jarNameEndIndex);
                if (javaFilePkgNs != null && !javaFilePkgNs.isEmpty() && Arrays.stream(excludePkgNs).anyMatch(javaFilePkgNs::startsWith)) {
                    return;
                }
            }
        }
        try (BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
            String line;
            int lineNumber = 0;
            List<TokenFileOccurrence.TokenOccurrence> fileLevelCollect = new ArrayList<>();

            while ((line = reader.readLine()) != null) {
                lineNumber++;
                String finalLine = line;
                int finalLineNumber = lineNumber;
                List<TokenFileOccurrence.TokenOccurrence> collect = Arrays.stream(searchFor).map(s -> {
                            if (finalLine.contains(s)) {
                                return new TokenFileOccurrence.TokenOccurrence(s, finalLineNumber);
                            }
                            return null;
                        })
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());
                if (!collect.isEmpty()) {
                    fileLevelCollect.addAll(collect);
                }
            }

            if (!fileLevelCollect.isEmpty()) {
                TokenFileOccurrence tokenFileOccurrence = new TokenFileOccurrence(absPathStr, tenantName, jarPath, jarName, fileLevelCollect);
                new Gson().toJson(tokenFileOccurrence);
                logger.info("{}", new Gson().toJson(tokenFileOccurrence));
            }

        } catch (IOException e) {
            //logger.error("Error reading the file: {}", path.toAbsolutePath());
        }
    }
}